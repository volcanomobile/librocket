LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := Rocket-prebuilt
LOCAL_SRC_FILES := $(TARGET_ARCH_ABI)/libRocket.a
LOCAL_STATIC_LIBRARIES := freetype2-prebuilt
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../../Include
TARGET_PLATFORM = android-9
include $(PREBUILT_STATIC_LIBRARY)

$(call import-module,freetype2/lib/android)

